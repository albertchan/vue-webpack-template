# vue-webpack-template

> A full-featured Webpack + vue-loader setup with hot reload, linting, testing & css extraction.

## Features

- Vue
- vue-router
- vuex
- vuex-router-sync
- Single-file Vue components
  - Hot-reload in development
  - CSS extraction for production

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
