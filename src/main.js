// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import App from './App';
import router from './router';
import store from './store';

// sync the router with the vuex store.
// this registers `store.state.route`
sync(store, router);

const app = new Vue({
  el: '#app',
  router,
  store,
  ...App,
});

export { app, router, store };
